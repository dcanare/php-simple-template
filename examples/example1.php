<?php
	include_once('../simple-template.php');
	$template = new HTMLTemplate('greenlightgo-template.html');
	$template->addJS('example1.js');
	$template->replace('TITLE', 'Example Page #1');
	$template->bufferStart();
?>

<h1>This is the first example :)</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque auctor nisl sem, sed aliquam lorem consequat quis. Etiam blandit fermentum massa, et tristique nisl. Nunc imperdiet mauris justo, ac sagittis ipsum porttitor et. Vivamus elementum elit at pulvinar viverra. Nam erat turpis, pellentesque nec sapien dapibus, aliquet molestie purus. Mauris pellentesque purus quam, a laoreet leo porttitor sed. Morbi vulputate fringilla velit vitae iaculis. Sed quis mauris in eros eleifend tristique. Nam tempus arcu velit. Phasellus quis lacus nulla. Nunc vulputate est a dui elementum pellentesque. Aenean commodo laoreet malesuada. Sed interdum condimentum orci eu ullamcorper. Fusce et velit orci.</p>
<p>Integer tincidunt lacus lectus, sed elementum arcu ullamcorper vitae. Sed faucibus nisl eget ornare vehicula. Proin sodales risus eu tellus aliquam egestas. Cras consequat sit amet lacus ac pharetra. Curabitur eu rhoncus erat. Quisque ut massa sollicitudin, cursus dui sed, eleifend augue. Vivamus rhoncus non sapien vel tincidunt. Integer sodales diam vel scelerisque cursus. Vestibulum tristique non lacus et pretium. Donec sollicitudin congue ornare. Aliquam et lacinia sem. Praesent porttitor iaculis magna a aliquam. Aliquam ornare, tellus pretium pharetra sollicitudin, arcu leo gravida magna, eget aliquet justo risus id dui. Fusce porta diam eget ante porta, in molestie metus eleifend. Nunc gravida quam libero.</p>
<p>Nunc ut urna convallis, cursus nisi vel, convallis orci. Nam sollicitudin sit amet quam ut porttitor. Donec eros sapien, porta at risus vel, imperdiet molestie mauris. Donec ullamcorper rutrum porttitor. Phasellus eget sapien laoreet, gravida sapien quis, dapibus sapien. Vestibulum posuere ullamcorper nisl quis semper. In eget ultricies dolor. Pellentesque fermentum vestibulum risus et suscipit. Nullam a velit leo.</p>

<p style="text-align: right"><a href="example2.php">Next &gt;</a></p>

<?php $template->bufferStop('PAGE_CONTENT'); ?>
