<?php
	include_once('../simple-template.php');
	$template = new HTMLTemplate('template.html', 'example2.css');
	$template->replace('TITLE', 'Example Page #2');
	$template->bufferStart();
?>

<h1>This is the second example :)</h1>

<p>Cras felis neque, posuere at tellus vel, placerat tristique mi. Nullam dolor erat, condimentum in lorem sit amet, adipiscing mattis diam. Nulla molestie convallis convallis. Mauris auctor mauris a lorem eleifend, eu feugiat sapien tincidunt. Donec commodo, enim vitae tempus iaculis, odio mi dignissim dui, id euismod orci metus at felis. Nunc dignissim libero in faucibus ullamcorper. In hac habitasse platea dictumst. Pellentesque commodo nec sem a tincidunt. Nam non nisi ut erat mollis cursus. Praesent molestie odio libero, non sagittis est tristique eget. Nam porttitor ipsum at sapien tempor tincidunt. Sed dolor diam, vulputate vel rutrum in, varius id orci. Sed vitae iaculis justo. Nunc cursus suscipit nunc non dictum. Fusce faucibus, magna aliquet mollis scelerisque, magna nibh gravida erat, quis eleifend nisi orci eu dui. Maecenas tincidunt egestas aliquam.</p>
<p>Integer gravida velit orci, sit amet tincidunt nibh facilisis sit amet. Integer facilisis gravida accumsan. Cras felis libero, tincidunt sed tellus a, tempus pellentesque orci. Nam rutrum lectus id tristique eleifend. Donec ultrices mi eu ante ultricies, nec lacinia diam interdum. Suspendisse viverra est quis aliquam lobortis. Nunc quam justo, tincidunt at felis sit amet, cursus aliquam diam.</p>
<p>The end.</p>

<p><a href="example1.php">&lt; Previous</a></p>

<?php $template->bufferStop('PAGE_CONTENT'); ?>
