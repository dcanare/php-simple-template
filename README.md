# PHP Simple Template #
A simple, somewhat minimal PHP templating system

Written by [Dominic Canare](mailto:dom@greenlightgo.org)

## Quickstart ##
### 1. Create a template file with {{TAGS}} ###
```
#!html
<!-- template.html -->
<title>{{TITLE}}</title>
<div id="header">HEADER</div>
<div id="content">{{CONTENT}}</div>
<div id="footer">FOOTER</div>
```
### 2. Create a PHP file ###
You can do small replacements using the **replace** function, and larger replacements using the buffer. Both are shown below.
```
#!php
<?php
	// index.php
	include_once('simple-template.php');
	$template = new HTMLTemplate('template.html');
	$template->replace('TITLE', 'My Page Title');
	$template->bufferStart();
?>
	Hello, World! Lorem ipsum, blah blah blah, some very long markup goes here.
<?php
	$template->bufferStop('CONTENT');
```
* Note: if you don't use the buffer system, you will need to call **$template->render()** to see anything.
### 3. Behold your output ###
```
#!html
<!-- template.html -->
<title>My Page Title</title>
<div id="header">HEADER</div>
<div id="content">Hello, World! Lorem ipsum, blah blah blah</div>
<div id="footer">FOOTER</div>
```