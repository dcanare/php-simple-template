<?php
/*
	The MIT License (MIT)

	Copyright (c) 2014 GreenLightGo.org and Dominic Canare

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

/**
 * A simple, somewhat minimal PHP templating system
 * @version 1.0
 * @author Dominic Canare <dom@greenlightgo.org>
 * @license MIT
 */

/**
 * BasicTemplate
 * A basic string-based template
 **/
class BasicTemplate {
	/**
	 * Create a new BasicTemplate object
	 *
	 * The template should include tags enclosed in 'stache brackets.
	 * E.g., {{TITLE}} or {{PAGE_NUMBER}} of {{PAGE_COUNT}}
	 * 
	 * @param	string	$template	The template - this can either be a file or text
	 **/
	function __construct($template) {
		$this->template = file_get_contents($template);
		if($this->template === false){
			$this->template = $template;
		}
		$this->replacements = array();
	}

	/**
	 * Perform a template replacement
	 * 
	 * @param	string match		The key to find (without the {{ and }} indicators)
	 * @param	string text			The value to drop in place of that key
	 * @param	boolean applyNow	Apply the replacement to the template immediately, or wait until render (default false)
	 * 
	 * @return	BasicTemplate	Returns itself for function chaining
	 **/
	function replace($match, $text, $applyNow=false){
		if($applyNow){
			$this->template = str_replace('{{'.$match.'}}', $text, $this->template);
		}else{
			$this->replacements[$match] = $text;
		}
		return $this;
	}

	/**
	 * Process replacements and render the result
	 * @param	boolean	$stripUnused	true to remove unsed template tags
	 **/
	function render($stripUnused=true){
		foreach($this->replacements as $key=>$value){
			$this->replace($key, $value, true);
		}

		if($stripUnused){
			$this->template = preg_replace('/\{\{[^\}]*\}\}/s', '', $this->template);
		}
		
		echo $this->template;
	}

	/**
	 * Begin output buffering
	 * @see bufferStop
	 **/
	function bufferStart(){
		ob_start();
	}
	
	/**
	 * Stop output buffering, drop buffer contents into the template, and render the result
	 * @param	string	$tagName	Indicates which template tag should receive buffer contents
	 * @see bufferStart
	 **/
	function bufferStop($tagName){
		$buffer = ob_get_contents();
		ob_end_clean();
		
		$this->replace($tagName, $buffer);
		$this->render();
	}
}


/**
 * HTMLTemplate
 * Extends BasicTemplate by adding CSS and JavaScript file linking
 **/
class HTMLTemplate extends BasicTemplate{
	/**
	 * Create a new HTMLTemplate object
	 *
	 * The template should include tags enclosed in 'stache brackets.
	 * E.g., {{TITLE}} or {{PAGE_NUMBER}} of {{PAGE_COUNT}}
	 *
	 * The template should specifically include {{ADDITONAL_CSS}} and {{ADDITIONAL_JS}} tags.
	 * 
	 * @param template	The template itself (not a file, but the contents of a file perhaps)
	 * @param bonusCSS	The web path to a CSS file to include along with whatever's already in the template
	 * 					This can be an array of files
	 * @param bonusJS	The web path to a JS file to include along with whatever's already in the template
	 * 					This can be an array of files
	 **/
	function __construct($template, $bonusCSS=null, $bonusJS=null) {
		parent::__construct($template);

		$this->bonusCSS = ($bonusCSS == null ? array() : $bonusCSS);
		$this->bonusJS = ($bonusJS == null ? array() : $bonusJS);
		
		if(!is_array($this->bonusCSS)) $this->bonusCSS = array($bonusCSS);
		if(!is_array($this->bonusJS)) $this->bonusJS = array($bonusJS);
	}

	/**
	 * Add additional CSS files to be included in the output
	 * @param	string	$cssURL	The (web client) path of the file to be added
	 * @return	HTMLTemplate	Returns itself for function chaining
	 **/
	function addCSS($cssURL){
		$this->bonusCSS[] = $cssURL;
		return $this;
	}

	/**
	 * Add additional javascript files to be included in the output
	 * @param	string	$jsURL	The (web client) path of the file to be added
	 * @return	HTMLTemplate	Returns itself for function chaining
	 **/
	function addJS($jsURL){
		$this->bonusJS[] = $jsURL;
		return $this;
	}

	/**
	 * Process replacements and render the result
	 * @param boolean $stripUnused true to remove unsed template tags
	 **/
	function render($stripUnused=true){
		$buffer = '';
		foreach($this->bonusCSS as $f){
			$buffer .= "<link href=\"$f\" rel=\"stylesheet\" type=\"text/css\" />\n\t\t";
		}
		$this->replace('ADDITIONAL_CSS', $buffer);
		
		$buffer = '';
		foreach($this->bonusJS as $f){
			$buffer .= "<script type=\"text/javascript\" src=\"$f\"></script>\n\t\t";
		}
		$this->replace('ADDITIONAL_JAVASCRIPT', $buffer);
		
		return parent::render($stripUnused);
	}
}
